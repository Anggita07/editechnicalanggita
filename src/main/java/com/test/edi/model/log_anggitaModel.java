package com.test.edi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "T_log_Anggita")
public class log_anggitaModel {
	
	@Id
	@Column(name="IDREQUESTBOOKING")
	private Character idReq;
	
	@Column(name="IDPLATFORM")
	private Character idPlat;
	
	@Column(name="NAMAPLATFORM")
	private Character namaPlat;
	
	@Column(name="DOCTYPE")
	private Character docType;
	
	@Column(name="TERMOFPAYMENT")
	private Character termOf;
	
	public Character getIdReq() {
		return idReq;
	}

	public void setIdReq(Character idReq) {
		this.idReq = idReq;
	}

	public Character getIdPlat() {
		return idPlat;
	}

	public void setIdPlat(Character idPlat) {
		this.idPlat = idPlat;
	}

	public Character getNamaPlat() {
		return namaPlat;
	}

	public void setNamaPlat(Character namaPlat) {
		this.namaPlat = namaPlat;
	}

	public Character getDocType() {
		return docType;
	}

	public void setDocType(Character docType) {
		this.docType = docType;
	}

	public Character getTermOf() {
		return termOf;
	}

	public void setTermOf(Character termOf) {
		this.termOf = termOf;
	}

	
	
}

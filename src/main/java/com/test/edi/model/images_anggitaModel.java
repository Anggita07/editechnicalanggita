package com.test.edi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="T_images_Anggita")
public class images_anggitaModel {
	
	@Id
	@Column(name="IDREQUESTBOOKING")
	private Character idReq;
	
	@Column(name="DESCRIPTION")
	private Character desc;

	public Character getDesc() {
		return desc;
	}

	public void setDesc(Character desc) {
		this.desc = desc;
	}

	public Character getIdReq() {
		return idReq;
	}

	public void setIdReq(Character idReq) {
		this.idReq = idReq;
	}
	

}

package com.test.edi.controller.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.test.edi.model.log_anggitaModel;
import com.test.edi.service.TestService;

@RestController
@RequestMapping("/api/apiTest")
public class testApi {
	
	@Autowired
	private TestService testService;
	
	@PostMapping("/post/test")
	@ResponseStatus(code = HttpStatus.CREATED)
	public Map<String, Object> postApi(@RequestBody log_anggitaModel log_anggitaModel){
		testService.create(log_anggitaModel);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("Success", Boolean.TRUE);
		map.put("Notifikasi", "Data Sudah di Input");
		
		return map;
	}
	
	@GetMapping("/get/test")
	@ResponseStatus(code = HttpStatus.OK)
	public List<log_anggitaModel> getApi(){
		List<log_anggitaModel> log_anggitaModelList = new ArrayList<log_anggitaModel>();
		log_anggitaModelList = testService.read();
		return log_anggitaModelList;
	}
	
	@GetMapping("/get/test/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	public List<log_anggitaModel> getId(){
		List<log_anggitaModel> log_anggitaModelList = new ArrayList<log_anggitaModel>();
		log_anggitaModelList = testService.read();
		return log_anggitaModelList;
	}
	
	@PutMapping("/put/test")
	@ResponseStatus(code = HttpStatus.OK)
	public Map<String, Object> putApi (@RequestBody log_anggitaModel log_anggitaModel){
		testService.update(log_anggitaModel);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("Success", Boolean.TRUE);
		map.put("Notifikasi", "Data Berhasil di Update");
		
		return map;
	}
		
	@DeleteMapping("/delete/test/{id}")
	@ResponseStatus()
	public Map<String, Object> deleteApi(@PathVariable Integer id){
		testService.delete(id);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("Success", Boolean.TRUE);
		map.put("Notifikasi", "Data berhasil di hapus");
		
		return map;
	}
	
	
	
	}
	
	



package com.test.edi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EdiTestAnggitaApplication {

	public static void main(String[] args) {
		SpringApplication.run(EdiTestAnggitaApplication.class, args);
	}

}

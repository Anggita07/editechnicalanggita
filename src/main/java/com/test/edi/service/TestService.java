package com.test.edi.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.edi.model.log_anggitaModel;
import com.test.edi.repository.TestRepository;

@Service
@Transactional
public class TestService {
	
	@Autowired
	private TestRepository testRepository;
	
	public void create(log_anggitaModel log_anggitaModel) {
		testRepository.save(log_anggitaModel);
	}
	
	public List<log_anggitaModel> read(){
		return testRepository.findAll();
	}
	
	public void readId(Integer id) {
		testRepository.findById(id);
	}
	
	public void update(log_anggitaModel log_anggitaModel) {
		testRepository.save(log_anggitaModel);
	}
	
	public void delete(Integer id) {
		testRepository.deleteById(id);
	}

}

package com.test.edi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.test.edi.model.log_anggitaModel;

public interface TestRepository extends JpaRepository<log_anggitaModel, Integer> {

	@Query("SELECT T FROM TestModel T WHERE T.name = ?1")
	log_anggitaModel cariNama(String name);
}
